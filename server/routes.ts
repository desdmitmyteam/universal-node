import * as path from 'path';
import * as express from 'express';
import { config } from './config/environment';
import { authRoutes } from './auth/auth.routes';
import { apiRoutes } from './api/api.routes';
// import errors from './components/errors';

/**
 * Set up main routes of back-end
 * @param app
 */
export function routes(app) {

  // app.get('/api/**', (req, res) => {
  //  res.send('api works');
  // });

  app.use('/api', apiRoutes);
  app.use('/auth', authRoutes);

  // Server static files from /browser
  app.get('*.*', express.static(path.join(config.distFolder, 'browser'), {maxAge: '1y'}));

  // todo: nove to angular routing
  // All undefined asset or api routes should return a 404
  // app.route('/:url(api|auth|components|app|bower_components|assets)/*')
  //  .get(errors[404]);

  // All other routes should redirect to the index.html
  // app.route('/*')
  //  .get((req, res) => {
  //    res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
  //  });
}
