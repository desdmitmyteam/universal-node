import * as jwt from 'jsonwebtoken';
import * as expressJwt from 'express-jwt';
import * as compose from 'composable-middleware';
import { User } from '../api/user/user.model';
import { config } from '../config/environment';
import { Roles } from '../common/roles';

const validateJwt = expressJwt({
  secret: config.secrets.session
});

enum Provider {
  Local = 'local',
  Google = 'google',
  Twitter = 'twitter',
  FaceBook = 'facebook'
}

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
const isAuthenticated = () => compose()
// Validate jwt
  .use((req, res, next) => {
    // allow access_token to be passed through query parameter as well
    if (req.query && req.query.hasOwnProperty('access_token')) {
      req.headers.authorization = `Bearer ${req.query.access_token}`;
    }

    // IE11 forgets to set Authorization header sometimes. Pull from cookie instead.
    if (req.query && typeof req.headers.authorization === 'undefined') {
      req.headers.authorization = `Bearer ${req.cookies.token}`;
    }

    validateJwt(req, res, next);
  })
  // Attach user to request
  .use((req, res, next) => {
    // todo: remove exec
    User.findById(req.user._id).exec()
      .then((user) => {
        if (!user) {
          return res.status(401).end();
        }

        req.user = user;
        next();

        return null;
      })
      .catch(next);
  });

/**
 * Checks if the user role meets the minimum requirements of the route
 */
const hasRole = (roleRequired: Roles) => {
  if (!roleRequired) {
    throw new Error('Required role needs to be set');
  }

  return compose()
    .use(isAuthenticated())
    .use((req, res, next) =>
      req.user.role === roleRequired || req.user.role === Roles.Admin
        ? next()
        : res.status(403).send('Forbidden'));
};

/**
 * Returns a jwt token signed by the app secret
 */
const signToken = (id, role) =>
  jwt.sign({_id: id, role}, config.secrets.session, {
    expiresIn: config.tokenExpires
  });

/**
 * Set token cookie directly for oAuth strategies
 */
const setTokenCookie = (req, res) => {
  if (!req.user) {
    return res.status(404).send('It looks like you aren\'t logged in, please try again.');
  }

  const token = signToken(req.user._id, req.user.role);
  res.cookie('token', token);
  res.redirect('/');
};

export { Provider, isAuthenticated, hasRole, signToken, setTokenCookie };
