import { use } from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';

export class Passport {

  public static setup(User) {
    use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password'
    }, function(email, password, done) {
      return Passport.localAuthenticate(User, email, password, done);
    }));
  }

  private static localAuthenticate(User, email, password, done) {
    User.findOne({
      email: email.toLowerCase()
    }).exec()
      .then((user) => {
        if (!user) {
          return done(null, false, {
            message: 'This email is not registered.'
          });
        }
        user.authenticate(password, function(authError, authenticated) {
          if (authError) {
            return done(authError);
          }
          if (!authenticated) {
            return done(null, false, {message: 'This password is not correct.'});
          } else {
            return done(null, user);
          }
        });
      })
      .catch(done);
  }
}
