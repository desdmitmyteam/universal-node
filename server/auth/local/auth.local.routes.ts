import { authenticate } from 'passport';
import { signToken } from '../auth.service';
import { Router } from 'express';

export const authLocalRoutes = Router().post('/', function(req, res, next) {
  authenticate('local', function(err, user, info) {
    const error = err || info;

    if (error) {
      return res.status(401).json(error);
    }

    if (!user) {
      return res.status(404).json({message: 'Something went wrong, please try again.'});
    }

    res.json({token: signToken(user._id, user.role)});
  })(req, res, next);
});
