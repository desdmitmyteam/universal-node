import { User } from '../api/user/user.model';
import { Passport } from './local/passport';
import { authLocalRoutes } from './local/auth.local.routes';
import { Router } from 'express';

Passport.setup(User);

export const authRoutes = Router().use('/local', authLocalRoutes);
