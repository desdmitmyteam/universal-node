import { Facade } from './facade';
import { Document } from 'mongoose';
import { applyPatch } from 'fast-json-patch';

export class Controller<T extends Document> {

  public constructor(public facade: Facade<T>) {
  }

  public static handleEntityNotFound(res) {
    return (entity) => {
      if (!entity) {
        res.sendStatus(404).end();
      }

      return entity;
    };
  }

  public static handleNoPermission(req, res, guard: (req, entity: any) => boolean) {
    return (entity) => {
      if (guard && !guard(req, entity)) {
        res.sendStatus(403).end();
      }

      return entity;
    };
  }

  public static respondWithResult(res, statusCode = 200, getter: (T) => any = (entity) => entity) {
    return (entity) => {
      if (entity) {
        return res.status(statusCode).json(getter(entity));
      }

      return entity;
    };
  }

  public static validationError(res) {
    return Controller.handleError(res, 422);
  }

  public static handleError(res, statusCode = 500) {
    return (err) => {
      res.status(statusCode).send(err);
    };
  }

  public create({req, res, guard}: { req, res, guard?(req): boolean }) {
    return Controller.handleNoPermission(req, res, guard)(Promise.resolve())
      .then(() => this.facade.create(req.body))
      .then(Controller.respondWithResult(res, 201))
      .catch(Controller.handleError(res));
  }

  public all({req, res, guard}: { req, res, guard?(req, entity: T[]): boolean }) {
    return this.facade.find()
      .then(Controller.handleNoPermission(req, res, guard))
      .then(Controller.respondWithResult(res))
      .catch(Controller.handleError(res));
  }

  public find({req, res, guard}: { req, res, guard?(req, entity: T[]): boolean }) {
    return this.facade.find(req.query)
      .then(Controller.handleNoPermission(req, res, guard))
      .then(Controller.respondWithResult(res))
      .catch(Controller.handleError(res));
  }

  public findOne({req, res, guard}: { req, res, guard?(req, entity: T): boolean }) {
    return this.facade.findOne(req.query)
      .then(Controller.handleNoPermission(req, res, guard))
      .then(Controller.respondWithResult(res))
      .catch(Controller.handleError(res));
  }

  public findById({req, res, guard, getter}: { req, res, guard?(req, entity: T): boolean, getter?(T): any }) {
    return this.facade.findById(req.params.id)
      .then(Controller.handleEntityNotFound(res))
      .then(Controller.handleNoPermission(req, res, guard))
      .then(Controller.respondWithResult(res, 200, getter))
      .catch(Controller.handleError(res));
  }

  // todo: should be empty
  public update({req, res, guard}: { req, res, guard?(req, entity: T): boolean }) {
    return this.facade.findById(req.params.id)
      .then(Controller.handleEntityNotFound(res))
      .then(Controller.handleNoPermission(req, res, guard))
      .then(() => this.facade.update({_id: req.params.id}, req.body))
      .then((results) => {
        if (results.n < 1) {
          return res.sendStatus(404);
        }

        if (results.nModified < 1) {
          return res.sendStatus(304);
        }

        res.sendStatus(204);
      })
      .catch(Controller.handleError(res));
  }

  public upsert({req, res, guard}: { req, res, guard?(req, entity: T): boolean }) {
    if (req.body._id) {
      Reflect.deleteProperty(req.body, '_id');
    }

    return this.facade.findById(req.params.id)
      .then(Controller.handleEntityNotFound(res))
      .then(Controller.handleNoPermission(req, res, guard))
      .then(() => this.facade.upsert({_id: req.params.id}, req.body, {
        new: true,
        upsert: true,
        setDefaultsOnInsert: true,
        runValidators: true
      }))
      .then(Controller.respondWithResult(res))
      .catch(Controller.handleError(res));
  }

  public patch({req, res, guard}: { req, res, guard?(req, entity: T): boolean }) {
    if (req.body._id) {
      Reflect.deleteProperty(req.body, '_id');
    }

    return this.facade.findById(req.params.id)
      .then(Controller.handleEntityNotFound(res))
      .then(Controller.handleNoPermission(req, res, guard))
      .then((entity) => {
        try {
          applyPatch(entity, req.body, true);
        } catch (err) {
          return Promise.reject(err);
        }

        return entity.save();
      })
      .then(Controller.respondWithResult(res))
      .catch(Controller.handleError(res));
  }

  // todo: should be empty
  public remove({req, res, guard}: { req, res, guard?(req, entity: T): boolean }) {
    return this.facade.findById(req.params.id)
      .then(Controller.handleEntityNotFound(res))
      .then(Controller.handleNoPermission(req, res, guard))
      .then(() => this.facade.remove({_id: req.params.id}))
      .then(() => res.sendStatus(204))
      .catch(Controller.handleError(res));
  }
}
