export interface IModel {
  _id?: string;
  __v?: number;
}
