export const user = {
  firstName: {
    required: 'First name is required'
  },
  lastName: {
    required: 'Last name is required'
  },
  email: {
    required: 'Email is required',
    shape: 'Please enter a valid email address',
    unique: 'The specified email address is already in use'
  } ,
  password: {
    requiredSimple: 'Password is required',
    required: 'Password must be between 8 and 128 characters',
    invalid: 'Invalid password',
    changed: 'Password successfully changed'
  },
  confirmPassword: {
    match: 'Please confirm your password. Passwords must match'
  }
};

export const record = {
  date: {
    required: 'Date is required'
  },
  distance: {
    required: 'Distance is required. Supposed to be a Number only'
  },
  period: {
    required: 'Time is required. Supposed to be in 00:00:00 format'
  }
}