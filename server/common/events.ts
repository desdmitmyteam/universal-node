import {EventEmitter} from 'events';
import { Schema } from 'mongoose';

export class Events {

  public emitter = new EventEmitter();
  private events = {
    save: 'save',
    remove: 'remove'
  };

  public constructor(schema: Schema) {

    // Set max event listeners (0 == unlimited)
    this.emitter.setMaxListeners(0);

    for (const e in this.events) {
      const event = this.events[e];
      schema.post(e, this.emitEvent(event));
    }
  }

  private emitEvent(event) {
    return (doc) => {
      this.emitter.emit(`${event}:${doc._id}`, doc);
      this.emitter.emit(event, doc);
    };
  }
}
