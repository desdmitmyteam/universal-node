import { Model, Document, ModelFindOneAndUpdateOptions } from 'mongoose';

export class Facade<T extends Document> {
  public constructor(private model: Model<T>) {
  }

  public create(body: T): Promise<T> {
    const model = new this.model(body);

    return model.save();
  }

  public find(...args): Promise<Array<T>> {
    return this.model
      .find(...args)
      .exec();
  }

  public findOne(...args): Promise<T> {
    return this.model
      .findOne(...args)
      .exec();
  }

  public findById(id: object|string|number, ...args): Promise<T> {
    return this.model
      .findById(id, ...args)
      .exec();
  }

  public update(query: object, doc: object, ...args): Promise<any> {
    return this.model
      .update(query, doc, ...args)
      .exec();
  }

  public upsert(
      query: object,
      doc: object,
      options: ModelFindOneAndUpdateOptions, ...args): Promise<T | null> {

    return this.model
      .findOneAndUpdate(query, doc, options, ...args)
      .exec();
  }

  public remove(query: object, ...args): Promise<void> {
    return this.model
      .remove(query, ...args)
      .exec();
  }
}
