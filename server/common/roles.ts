export enum Roles {
  Guest = 'guest',
  User = 'user',
  Admin = 'admin'
}