import { Facade } from '../../common/facade';
import { Controller } from '../../common/controller';
import { IUserSchema, User } from './user.model';
import { Provider } from '../../auth/auth.service';
import { config } from '../../config/environment';
import * as _ from 'lodash';
import * as jwt from 'jsonwebtoken';

export class UserController extends Controller<IUserSchema> {

  /**
   * Get list of users
   */
  public all({req, res}) {
    // never give out the password or salt
    return this.facade.find({}, '-salt -password')
      .then(Controller.respondWithResult(res))
      .catch(Controller.handleError(res));
  }

  /**
   * Get a single user
   */
  public findById({req, res}) {
    return super.findById({req, res, getter: (user) => user.profile});
  }

  /**
   * Creates a new user
   */
  public create({req, res}) {
    return this.facade.create(_.extend(req.body, {provider: Provider.Local}))
      .then((user) => {
        const token = jwt.sign({_id: user._id}, config.secrets.session, {
          expiresIn:  config.tokenExpires
        });
        res.json({token});
      })
      .catch(Controller.validationError(res));
  }

  /**
   * Change a users password
   */
  public changePassword({req, res}) {
    const oldPass = String(req.body.oldPassword);
    const newPass = String(req.body.newPassword);

    User.findById(req.user._id).exec()
      .then((user) => {
        if (!user) {
          return res.status(401).end();
        }

        if (req.user.authenticate(oldPass)) {
          req.user.password = newPass;

          return req.user.save()
            .then(() => {
              res.status(204).end();
            });
        } else {
          return res.status(403).send({errors: {oldPassword: {message: 'Old password is not right'}}});
        }
      })
      .catch(Controller.validationError(res));
  }

  /**
   * Get my info
   */
  public me({req, res}) {
    const userId = req.user._id;

    // never give out the password or salt
    return this.facade.findOne({_id: userId}, '-salt -password')
      .then(Controller.handleEntityNotFound(res))
      .then(Controller.respondWithResult(res))
      .catch(Controller.handleError(res));
  }
}

export const userController = new UserController(new Facade<IUserSchema>(User));
