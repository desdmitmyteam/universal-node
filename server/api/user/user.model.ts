import { Model, model, Schema, Document, SchemaDefinition } from 'mongoose';
import * as crypto from 'crypto';
import { Events } from '../../common/events';
import { IModel } from '../../common/model';
import { Roles } from '../../common/roles';
import { user } from '../../common/messages';
import * as isEmail from 'validator/lib/isEmail';
import * as beautifyUnique from 'mongoose-beautiful-unique-validation';

export interface IUser extends IModel {
  firstName: string;
  lastName: string;
  email: string;
  role: string;
  password: string;
  provider: string;
  salt: string;
  profile: {
    name: string;
    rolse: string;
  };
  token: {
    _id: string;
    role: string;
  };
  authenticate(password: string, callback?: (authError, authenticated: boolean) => any): void;
  makeSalt(...args): string;
  encryptPassword(password, callback): string;
}

export type IUserSchema = IUser & Document;

const UserSchema = new Schema({
  firstName: {
    type: String,
    required: [true, user.firstName.required]
  },
  lastName: {
    type: String,
    required: [true, user.lastName.required]
  },
  email: {
    type: String,
    lowercase: true,
    trim: true,
    unique:  user.email.unique,
    required: [true, user.email.required],
    validate: [ isEmail, user.email.shape ]
  },
  role: {
    type: String,
    default: Roles.User
  },
  password: {
    type: String,
    required: [true, user.password.required]
  },
  provider: String,
  salt: String
});

// Add beautify error response for unique errors
UserSchema.plugin(beautifyUnique);

// Virtual: Public profile information
UserSchema
  .virtual('profile')
  .get(function() {
    return {
      name: this.name,
      role: this.role
    };
  });

// Virtual: Non-sensitive info we'll be put in the token
UserSchema
  .virtual('token')
  .get(function() {
    return {
      _id: this._id,
      role: this.role
    };
  });

const validatePresenceOf = (value) => value && value.length;

// Pre-save hook
UserSchema
  .pre('save', function(next) {
    // Handle new/update passwords
    if (!this.isModified('password')) {
      return next();
    }

    if (!validatePresenceOf(this.password)) {
      return next(new Error(user.password.invalid));
    }

    // Make salt with a callback
    this.makeSalt((saltErr, salt) => {
      if (saltErr) {
        return next(saltErr);
      }
      this.salt = salt;
      this.encryptPassword(this.password, (encryptErr, hashedPassword) => {
        if (encryptErr) {
          return next(encryptErr);
        }
        this.password = hashedPassword;

        return next();
      });
    });
  });

// Methods
UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} password
   * @param {Function} callback
   * @return {Boolean}
   * @api public
   */
  authenticate(password, callback) {
    if (!callback) {
      return this.password === this.encryptPassword(password);
    }

    this.encryptPassword(password, (err, pwdGen) => {
      if (err) {
        return callback(err);
      }

      if (this.password === pwdGen) {
        return callback(null, true);
      } else {
        return callback(null, false);
      }
    });
  },

  /**
   * Make salt
   *
   * @param {Number} [byteSize] - Optional salt byte size, default to 16
   * @param {Function} callback
   * @return {String}
   * @api public
   */
  makeSalt(...args) {
    const defaultByteSize = 16;
    let byteSize;
    let callback;

    if (typeof args[0] === 'function') {
      callback = args[0];
      byteSize = defaultByteSize;
    } else if (typeof args[1] === 'function') {
      callback = args[1];
    } else {
      throw new Error('Missing Callback');
    }

    if (!byteSize) {
      byteSize = defaultByteSize;
    }

    return crypto.randomBytes(byteSize, (err, salt) => {
      if (err) {
        return callback(err);
      } else {
        return callback(null, salt.toString('base64'));
      }
    });
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @param {Function} callback
   * @return {String}
   * @api public
   */
  encryptPassword(password, callback) {
    if (!password || !this.salt) {
      if (!callback) {
        return null;
      } else {
        return callback('Missing password or salt');
      }
    }

    const defaultIterations = 10000;
    const defaultKeyLength = 64;
    const salt = new Buffer(this.salt, 'base64');

    if (!callback) {
      return crypto.pbkdf2Sync(password, salt, defaultIterations, defaultKeyLength, 'sha256')
        .toString('base64');
    }

    return crypto.pbkdf2(password, salt, defaultIterations, defaultKeyLength, 'sha256', (err, key) => {
      if (err) {
        return callback(err);
      } else {
        return callback(null, key.toString('base64'));
      }
    });
  }
};

export const UserEvents = new Events(UserSchema).emitter;
export const User: Model<IUserSchema> = model('User', UserSchema);
