import { Router } from 'express';
import { hasRole, isAuthenticated } from '../../auth/auth.service';
import { userController } from './user.ctrl';
import { Roles } from '../../common/roles';

const router = Router();

router.route('/')
  .get(hasRole(Roles.Admin), (req, res) => userController.all({req, res}))
  .post((req, res) => userController.create({req, res}));

router.route('/me')
  .get(isAuthenticated(), (req, res) => userController.me({req, res}));

router.route('/:id')
  .get(isAuthenticated(), (req, res) => userController.findById({req, res}))
  .delete(hasRole(Roles.Admin), (req, res) => userController.remove({req, res}));

router.route('/:id/password')
  .put(isAuthenticated(), (req, res) => userController.changePassword({req, res}));

export const userRoutes = router;
