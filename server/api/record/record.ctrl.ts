import { Facade } from '../../common/facade';
import { IRecordSchema, Record } from './record.model';
import { Controller } from '../../common/controller';
import * as _ from 'lodash';
import moment = require('moment');

export class RecordController extends Controller<IRecordSchema> {

  /**
   * Get list of users
   */
  public all({req, res}) {
    req.query = {createdBy: req.user._id};
    return super.find({req, res});
  }

  public create({req, res}) {
    req.body.createdBy = req.user._id;
    return super.create({req, res});
  }

  public report({req, res}) {
    return this.facade.find({createdBy: req.user._id})
      .then((records) => {
        const items = _(records)
          .groupBy((rec) =>
            rec.date.getFullYear() + moment(rec.date).week())
          .transform((result, value, key) => {
            result.push({
              week: key,
              distance: _.round(_.sumBy(value, 'distance') / value.length),
              period: _.round(_.sumBy(value, 'period') / value.length)
            });
          }, [])
          .sortBy('week')
          .map((value, index) => {
            value.week = index + 1;
            return value;
          })
          .value();

        return items;
      })
      .then(Controller.respondWithResult(res))
      .catch(Controller.handleError(res));
  }
}

export const recordController = new RecordController(new Facade<IRecordSchema>(Record));
