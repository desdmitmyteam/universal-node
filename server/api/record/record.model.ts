import { Model, model, Schema, Document } from 'mongoose';
import { Events } from '../../common/events';
import { IModel } from '../../common/model';
import { record } from '../../common/messages';

export interface IRecord extends IModel {
  createdBy?: string;
  date: Date;
  distance: number;
  period: number;
}

export type IRecordSchema = IRecord & Document;

const RecordSchema = new Schema({
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    reqired: true
  },
  date: {
    type: Date,
    required: [true, record.date.required]
  },
  distance: {
    type: Number,
    required: [true, record.distance.required]
  },
  period: {
    type: Number,
    required: [true, record.period.required]
  }
});

export const RecordEvents = new Events(RecordSchema).emitter;
export const Record: Model<IRecordSchema> = model('Record', RecordSchema);
