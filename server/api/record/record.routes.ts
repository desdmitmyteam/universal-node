import * as express from 'express';
import { recordController } from './record.ctrl';
import { isAuthenticated } from '../../auth/auth.service';
import { IRecord } from './record.model';

const guard = (req, record: IRecord) => String(record.createdBy) === String(req.user._id);

const router = express.Router()
  .use(isAuthenticated());

router.route('/')
  .get((req, res) => recordController.all({req, res}))
  .post((req, res) => recordController.create({req, res}));

router.route('/report')
  .get((req, res) => recordController.report({req, res}));

router.route('/:id')
  .get((req, res) => recordController.findById({req, res, guard}))
  .put((req, res) => recordController.update({req, res, guard}))
  .patch((req, res) => recordController.patch({req, res, guard}))
  .delete((req, res) => recordController.remove({req, res, guard}));

export const recordRoutes = router;
