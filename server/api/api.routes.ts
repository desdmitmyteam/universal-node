import { Router } from 'express';
import { thingRoutes } from './thing/thing.routes';
import { userRoutes } from './user/user.routes';
import { recordRoutes } from './record/record.routes';

const router = Router();

router.use('/things', thingRoutes);
router.use('/records', recordRoutes);
router.use('/users', userRoutes);

export const apiRoutes = router;
