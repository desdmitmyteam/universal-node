import { Model, model, Schema, Document } from 'mongoose';
import { Events } from '../../common/events';
import { IModel } from '../../common/model';

export interface IThing extends IModel {
  name: string;
  info: string;
  active: boolean;
}

export type IThingSchema = IThing & Document;

const ThingSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});

export const ThingEvents = new Events(ThingSchema).emitter;
export const Thing: Model<IThingSchema> = model('Thing', ThingSchema);
