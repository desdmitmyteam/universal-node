import { Facade } from '../../common/facade';
import { IThingSchema, Thing } from './thing.model';
import { Controller } from '../../common/controller';

export class TingController extends Controller<IThingSchema> {
}

export const tingController = new TingController(new Facade<IThingSchema>(Thing));
