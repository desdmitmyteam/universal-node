import * as express from 'express';
import { tingController } from './thing.ctrl';
import { isAuthenticated } from '../../auth/auth.service';

const router = express.Router();

router.route('/')
  .get(isAuthenticated(), (req, res) => tingController.all({req, res}))
  .post((req, res) => tingController.create({req, res}));

router.route('/:id')
  .get((req, res) => tingController.findById({req, res}))
  .put((req, res) => tingController.update({req, res}))
  .patch((req, res) => tingController.patch({req, res}))
  .delete((req, res) => tingController.remove({req, res}));

export const thingRoutes = router;
