// import express from 'express';
import * as mongoose from 'mongoose';
import * as http from 'http';
import * as bluebird from 'bluebird';
import { config } from './config/environment';
import { expressConfig } from './config/express';

/**
 * this function starts the server
 * @param app
 * @returns {() => number | any}
 */
export function appServer(app) {

  (mongoose as any).Promise = bluebird;

  // Connect to MongoDB
  mongoose.connect(config.mongo.uri, config.mongo.options);
  mongoose.connection.on('error', function(err) {
    console.error(`MongoDB connection error: ${err}`);
    process.exit(-1);
  });

  // Populate databases with sample data
  if (config.seedDB) {
    require('./config/seed');
  }

  // Setup server
  const server = http.createServer(app);

  expressConfig(app);
  // registerRoutes(app);

  // Start server
  function startServer() {
    app.jogging = server.listen(config.port, config.ip, () => {
      console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
    });
  }

  return () => setImmediate(startServer);
}
