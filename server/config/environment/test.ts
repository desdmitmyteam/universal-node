// Test specific configuration
export const config = {
  seedDB: false,
  mongo: {
    uri: 'mongodb://localhost/jogging-app-test'
  }
};
