import * as path from 'path';

// Production specific configuration
export const config = {
  root: path.normalize(process.cwd()),
  ip: process.env.ip || undefined,
  port: process.env.PORT || 8080,
  seedDB: true,
  mongo: {
    uri: process.env.MONGODB_URI || 'mongodb://localhost/jogging-app'
  }
};
