// const packageJson = require('../../../package.json');

export const config = {
  name: 'jogging-app',
  env: (process.env.NODE_ENV === 'production' ? 'prod' : process.env.NODE_ENV) || 'develop',
  port: Number(process.env.PORT) || 5000,
  ip: (process.env.IP || '0.0.0.0') as string,
};
