import * as path from 'path';
import * as _ from 'lodash';
import { config as shared } from './shared';
import { config as prod } from './prod';
import { config as test } from './test';

// All configurations will extend these options
const all = {
  // Application name
  name: shared.name,
  // Current environment
  env: shared.env,
  // Root path of server
  root: path.normalize(`${__dirname}/../../..`),
  // Path to build folder
  distFolder: path.join(process.cwd(), 'dist'),
  // Server port
  port: shared.port,
  // Server IP
  ip: shared.ip,
  // Should we populate the DB with sample data?
  seedDB: true,
  // todo: hmmmm
  // Secret for session
  secrets: {
    session: 'jogging-secret'
  },
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/jogging-app-dev',
    options: {
      db: {
        safe: true
      },
      useMongoClient: true
    }
  },
  // Token longevity in seconds (default: 5 hours)
  tokenExpires: 60 * 60 * 5
};

const exported =
  (all.env === 'prod' && prod) ||
  (all.env === 'test' && test) ||
  {};

export const config = _.merge(all, exported);
