import * as favicon from 'serve-favicon';
import * as morgan from 'morgan';
import * as compression from 'compression';
import * as bodyParser from 'body-parser';
import * as methodOverride from 'method-override';
import * as cookieParser from 'cookie-parser';
import * as path from 'path';
import * as lusca from 'lusca';
import * as passport from 'passport';
import * as session from 'express-session';
import * as connectMongo from 'connect-mongo';
import * as mongoose from 'mongoose';
import { config } from './environment';

const MongoStore = connectMongo(session);

export function expressConfig(app) {
  const env = app.get('env');

  // if (env === 'develop' || env === 'test') {
  //  app.use(express.static(path.join(config.root, '.tmp')));
  // }

  if (env === 'prod') {
    // app.use(favicon(path.join(config.root, 'dist', 'browser', 'favicon.ico')));
  }

  // app.set('appPath', path.join(config.root, 'client'));
  // app.use(express.static(app.get('appPath')));
  app.use(morgan('dev'));

  // app.set('views', `${config.root}/server/views`);
  // app.engine('html', require('ejs').renderFile);
  // app.set('view engine', 'html');
  app.use(compression());
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(cookieParser());
  app.use(passport.initialize());

  // Persist sessions with MongoStore / sequelizeStore
  // We need to enable sessions for passport-twitter because it's an
  // oauth 1.0 strategy, and Lusca depends on sessions
  app.use(session({
    secret: config.secrets.session,
    saveUninitialized: true,
    resave: false,
    store: new MongoStore({
      mongooseConnection: mongoose.connection,
      db: 'jogging-app'
    })
  }));

  // Lusca - express server security
  // https://github.com/krakenjs/lusca
  // todo: remove `removed` in truly production mode
  if (env === 'removed' && env !== 'test' && env !== 'develop' && !process.env.SAUCE_USERNAME) {
    app.use(lusca({
      csrf:  {angular: true},
      xframe: 'SAMEORIGIN',
      hsts: {
        maxAge: 31536000, // 1 year, in seconds
        includeSubDomains: true,
        preload: true
      },
      xssProtection: true
    }));
  }

  // Error handler - has to be last
  if (['develop', 'test'].indexOf(env) !== -1) {
    const errorHandler = require('errorhandler');
    app.use(errorHandler());
  }
}
