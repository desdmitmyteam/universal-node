import { Thing } from '../api/thing/thing.model';
import { User } from '../api/user/user.model';
import { Record } from '../api/record/record.model';
import { Roles } from '../common/roles';

User.find({})
  .then((users) => {
    if (users.length) {
      throw Error('data already exist');
    }

    return users;
  })
  .then(() => User.find({}).remove())
  .then(() =>
    User.create({
        provider: 'local',
        firstName: 'gn',
        lastName: 'Test User',
        email: 'test@example.com',
        password: 'test'
      }, {
        provider: 'local',
        role: Roles.Admin,
        firstName: 'gn',
        lastName: 'Administrator',
        email: 'admin@example.com',
        password: 'admin'
      })
      .then(() => {
        console.log('finished populating users');
      })
  )
  .then(() =>
    Record.find({}).remove().then(() =>
      User.findOne({email: 'admin@example.com'}).exec((err, user) => {
        for (let i = 0; i < 80; i++) {
          Record.create({
            createdBy: user._id,
            date: (() => {
              const date = new Date();
              date.setDate(date.getDate() - (i % 40));
              return date;
            })(),
            distance: 100 * (((i + 1) % 10) + 1),
            period: 100000 * (((i + 1) % 4) + 1)
          });
        }
      }))
  )
  .then(() => Record.find({}).remove().then(() =>
    User.findOne({email: 'test@example.com'}).exec((err, user) => {
      for (let i = 40; i < 20; i++) {
        Record.create({
          createdBy: user._id,
          date: (() => {
            const date = new Date();
            date.setDate(date.getDate() - (i % 40));
            return date;
          })(),
          distance: 100 * (((i + 1) % 10) + 1),
          period: 100000 * (((i + 1) % 4) + 1)
        });
      }
    }))
  )
  .catch((err) => {
    console.log(err.message);
    return err;
  });
