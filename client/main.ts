import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

interface IPolyFillErrorConstructor extends ErrorConstructor {
  stackTraceLimit: any;
}

if (environment.production) {
  enableProdMode();
} else {
  (Error as IPolyFillErrorConstructor).stackTraceLimit = Infinity;
}

document.addEventListener('DOMContentLoaded', () => {
  platformBrowserDynamic().bootstrapModule(AppModule);
});
