import { Log } from 'ng2-logger';

export * from '../../server/config/environment/shared.js';

export const environment = {
  production: true
};

Log.setProductionMode();
