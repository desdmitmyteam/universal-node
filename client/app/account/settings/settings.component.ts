import { Component } from '@angular/core';
import { AuthService } from '../../common/auth/auth.service';
import { user } from '../../../../server/common/messages';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { NotifyType } from '../../utils/type';

@Component({
  selector: 'settings',
  templateUrl: './settings.component.html'
})
export class SettingsComponent {

  public message = user;
  public data = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: ''
  };

  // todo: toastr
  public submitted = false;

  public constructor(
      private authService: AuthService,
      private router: Router,
      private toaster: ToasterService) {
  }

  public changePassword(form) {
    if (form.invalid) {
      return;
    }

    this.submitted = true;

    return this.authService.changePassword(this.data).subscribe(
      () => {
        this.toaster.pop(NotifyType.Success, user.password.changed);
        this.data.oldPassword = '';
        this.data.newPassword = '';
        this.data.confirmPassword = '';
        this.router.navigate(['/']);
      }
    );
  }
}
