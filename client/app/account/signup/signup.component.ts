import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from '../../common/auth/auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { user } from '../../../../server/common/messages';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent {
  public data = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: ''
  };

  public message = user;

  public constructor(private authService: AuthService, private router: Router) {
  }

  public register(form) {
    return form.valid
      ? this.authService.createUser(this.data)
        .subscribe(() => {
          this.router.navigateByUrl('/');
        })
      : Observable.of(null);

  }
}
