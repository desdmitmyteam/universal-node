import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SettingsComponent } from './settings/settings.component';
import { SignupComponent } from './signup/signup.component';
import { NotAuthGuard } from '../common/auth/not-auth-guard.service';

export enum AccountRoute {
  Login = 'login',
  Settins = 'settings',
  Signup = 'signup'
}

export const AccountRoutes: Routes = [
  {path: AccountRoute.Login, component: LoginComponent, canActivate: [NotAuthGuard]},
  {path: AccountRoute.Settins, component: SettingsComponent},
  {path: AccountRoute.Signup, component: SignupComponent, canActivate: [NotAuthGuard]}
];
