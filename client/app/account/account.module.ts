import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SettingsComponent } from './settings/settings.component';
import { SignupComponent } from './signup/signup.component';
import { AccountRoute, AccountRoutes } from './account.routes';
import { CommonModule } from '@angular/common';
import { unauthorizeUser } from '../common/auth/auth.service';
import { ValidateEqualDerective } from '../utils/validate-equal.derective';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild(AccountRoutes)
  ],
  declarations: [
    LoginComponent,
    SignupComponent,
    SettingsComponent,
    ValidateEqualDerective
  ]
})
export class AccountModule {

  public constructor(router: Router) {
    unauthorizeUser.subscribe(
      () => router.navigateByUrl(`/${AccountRoute.Signup}`));
  }
}
