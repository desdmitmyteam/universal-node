import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../common/auth/auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})
export class LoginComponent {
  public data = {
    email: '',
    password: ''
  };
  // todo: use toaster
  public errors = {login: ''};
  public submitted = false;

  public constructor(private authService: AuthService, private router: Router) {
  }

  public login() {
    this.submitted = true;

    return this.authService.login(this.data).subscribe(
      () => this.router.navigateByUrl('/'),
      (err) => { this.errors.login = err.message; });
  }
}
