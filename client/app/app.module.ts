import { BrowserModule } from '@angular/platform-browser';
import {
  NgModule,
  ApplicationRef
} from '@angular/core';
import {
  HttpModule
} from '@angular/http';
import {
  removeNgStyles,
  createNewHosts,
  createInputTransfer
} from '@angularclass/hmr';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
import { AppComponent } from './app.component';
import { logger } from './utils/logger';
import { HomeModule } from './home/home.module';
import { CommonAppModule } from './common/common.module';
import { AccountModule } from './account/account.module';
import { AdminModule } from './admin/admin.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToasterModule } from 'angular2-toaster';
import { RecordsModule } from './records/records.module';
import { ReportsModule } from './reports/reports.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'my-app'}),
    RouterModule.forRoot(appRoutes, {enableTracing: process.env.NODE_ENV === 'development'}),
    HomeModule,
    CommonAppModule,
    AccountModule,
    AdminModule,
    RecordsModule,
    ReportsModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToasterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  public constructor(private appRef: ApplicationRef) {
    this.appRef = appRef;
  }

  public hmrOnInit(store) {
    if (!store || !store.state) {
      return;
    }

    logger.data(`HMR store ${store}`);
    logger.data(`store.state.data: ${store.state.data}`);
    // inject AppStore here and update it
    // this.AppStore.update(store.state)
    if ('restoreInputValues' in store) {
      store.restoreInputValues();
    }
    // change detection
    this.appRef.tick();
    Reflect.deleteProperty(store, 'state');
    Reflect.deleteProperty(store, 'restoreInputValues');
  }

  public hmrOnDestroy(store) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // inject your AppStore and grab state then set it on store
    // var appState = this.AppStore.get()
    store.state = {data: 'yolo'};
    // store.state = Object.assign({}, appState)
    // save input values
    store.restoreInputValues = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  public hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    Reflect.deleteProperty(store, 'disposeOldHosts');
  }
}
