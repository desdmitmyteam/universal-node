import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ApiComponent } from './api.component';

@NgModule({
  declarations: [
    ApiComponent
  ],
  imports: [
    RouterModule.forRoot([
      { path: '', component: ApiComponent, pathMatch: 'full'},
      { path: 'lazy', loadChildren: './lazy/lazy.module#LazyModule'}
    ])
  ]
})
export class ApiModule {
}
