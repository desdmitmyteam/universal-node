import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'api',
  template: `<h3>{{ message }}</h3>`
})
export class ApiComponent implements OnInit {
  public message: string;

  public ngOnInit() {
    this.message = 'This page supposed to show api description';
  }
}
