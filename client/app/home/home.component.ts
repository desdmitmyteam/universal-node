import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import { IThing } from '../../../server/api/thing/thing.model';
import * as _ from 'lodash';
import { HomeService } from './home.service';
import { logger } from '../utils/logger';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public things: Array<IThing>;
  public newThing: IThing;

  public constructor(private mainService: HomeService) {
  }

  public ngOnInit() {
    return this.mainService.all().subscribe((things) => {
      this.things = things;
    });
  }

  public addThing() {
    if (this.newThing.name) {
      return this.mainService.create(this.newThing).subscribe((thing) => {
        this.things.push(thing);
        this.newThing = HomeComponent.getNewThing();
        logger.data('Add Thing');
      });
    }
  }

  public removeThing(thing: IThing) {
    return this.mainService.remove(thing).subscribe(() => {
      _.remove(this.things, (item) => item._id === thing._id);
      logger.data('Deleted Thing');
    });
  }

  private static getNewThing(): IThing {
    return {
      name: '',
      info: '',
      active: true
    };
  }
}
