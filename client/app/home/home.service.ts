import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IThing } from '../../../server/api/thing/thing.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HomeService {

  public constructor(private http: HttpClient) {
  }

  public all(): Observable<IThing[]> {
    return this.http.get<IThing[]>('/api/things');
    // todo: intercept all errors
    // todo: remove angular-2-jwt
    // todo: remove angular/http
    // .catch((err) => Observable.throw(err.json().error || 'Server error'));
  }

  public create(thing: IThing): Observable<IThing> {
      return this.http.post<IThing>('/api/things', thing);
  }

  public remove(thing: IThing): Observable<void> {
    return this.http.delete<void>(`/api/things/${thing._id}`);
  }
}
