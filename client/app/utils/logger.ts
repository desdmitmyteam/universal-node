import { Log } from 'ng2-logger';
import { config } from '../../environments/environment';

export const logger = Log.create(config.name);
logger.color = 'mediumseagreen';
