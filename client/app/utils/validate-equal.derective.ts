import { Directive, forwardRef, Attribute, OnDestroy, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { logger } from './logger';
import { Subscription } from 'rxjs/Subscription';

@Directive({
  selector: '[validateEqual][formControlName],[validateEqual][formControl],[validateEqual][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ValidateEqualDerective),
      multi: true
    }
  ]
})
export class ValidateEqualDerective implements Validator, OnDestroy {

  private subscription: Subscription;

  public constructor(
    @Attribute('validateEqual') public validateEqual: string) {
  }

  public validate(control: AbstractControl): { [key: string]: any } {
    if (!this.subscription) {
      const target = control.root.get(this.validateEqual);
      this.subscription = target && target.valueChanges.subscribe(() => {
        logger.data('validateEqual', target.value);
        control.setErrors(this.check(control));
      });
    }

    return this.check(control);
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private check(control) {
    const value = control.value;
    const target = control.root.get(this.validateEqual);
    return target && target.value !== value
      ? {validateEqual: false}
      : null;
  }
}
