export enum NotifyType {
  Error = 'error',
  Info = 'info',
  Wait = 'wait',
  Success = 'success',
  Warning = 'warning'
}
