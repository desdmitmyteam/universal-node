import { Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AuthGuardAdmin } from '../common/auth/auth-guard-admin.service';

export const AdminRoutes: Routes = [{
  path: 'admin',
  component: AdminComponent,
  canActivate: [AuthGuardAdmin],
  canLoad: [AuthGuardAdmin]
}];
