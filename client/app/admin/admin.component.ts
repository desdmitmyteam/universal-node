import { Component, OnInit } from '@angular/core';
import { UserService } from '../common/auth/user.service';
import { IUser } from '../../../server/api/user/user.model';
import * as _ from 'lodash';

@Component({
  selector: 'admin',
  templateUrl: './admin.html',
  styleUrls: ['./admin.scss']
})
export class AdminComponent implements OnInit {

  public users: Array<IUser>;

  public constructor(private userService: UserService) {
  }

  public ngOnInit(): void {
    this.userService.all().subscribe((users) => {
      this.users = users;
    });
  }

  public removeUser(user: IUser) {
    this.userService.remove(user._id).subscribe((userId) => {
      _.remove(this.users, (item) => item._id === userId);
    });
  }
}
