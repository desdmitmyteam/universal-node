import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `
    <div class="container">
      <h1 class="text-center">Page not found</h1>
    </div>
  `,
  styles: [`
    .text-center {
        padding-top: 100px;
        padding-bottom: 100px;
    }
  `]
})
export class PageNotFoundComponent {
}
