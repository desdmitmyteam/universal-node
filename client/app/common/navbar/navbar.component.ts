import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService, IUserInfo } from '../auth/auth.service';
import { IUser } from '../../../../server/api/user/user.model';
import { Roles } from '../../../../server/common/roles';
import { Form, NgForm } from '@angular/forms';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent {

  public isCollapsed = true;
  public menu = [
    { title: 'Records', link: '/records' },
    { title: 'Reports', link: '/report/records' }
  ];

  public userInfo: IUserInfo = {
    user: null,
    isAuthorized: false
  };

  public model = {
    email: '',
    password: ''
  };

  public constructor(private authService: AuthService, private router: Router) {

    this.authService.currentUser.subscribe((userInfo) => {
      this.userInfo = userInfo;
    });
  }

  public login(form: NgForm) {
    if (form.valid) {
      return this.authService.login(this.model).subscribe(
        () => this.router.navigateByUrl('/'));
    }
  }

  public logout() {
    return this.authService.logout().subscribe(
      () => this.router.navigateByUrl('/signup')
    );
  }
}
