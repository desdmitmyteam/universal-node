import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class NotAuthGuard implements CanActivate, CanLoad {
  public constructor(private authService: AuthService) {
  }

  public canActivate() {
    return this.check();
  }

  public canLoad() {
    return this.check();
  }

  private check() {
    if (this.authService.isAuthorized) {
      this.authService.router.navigateByUrl('/');
    }

    return !this.authService.isAuthorized;
  }
}
