import { Injectable } from '@angular/core';
import { CanActivate, CanLoad } from '@angular/router';
import { AuthService } from './auth.service';
import { Roles } from '../../../../server/common/roles';

@Injectable()
export class AuthGuardUser implements CanActivate, CanLoad {
  public static parameters = [AuthService];

  public constructor(private authService: AuthService) {
  }

  public canActivate() {
    return this.authService.hasRole(Roles.User);
  }

  public canLoad() {
    return this.authService.hasRole(Roles.User);
  }
}
