import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IUser } from '../../../../server/api/user/user.model';
import 'rxjs/add/operator/catch';
import { HttpClient } from '@angular/common/http';

function handleError(err) {
  return Observable.throw(err.json().error || 'Server error');
}

@Injectable()
export class UserService {
  public constructor(private http: HttpClient) {
  }

  public all(): Observable<Array<IUser>> {
    return this.http.get<IUser[]>('/api/users/');
  }

  public get(user?: IUser): Observable<IUser> {
    return this.http.get<IUser>(`/api/users/${user ? user._id : 'me'}`);
  }

  public create(user: IUser): Observable<{token: string}> {
    return this.http.post<{token: string}>('/api/users/', user);
  }

  public changePassword(userId: string, oldPassword: string, newPassword: string): Observable<void> {
    return this.http.put<void>(`/api/users/${userId}/password`, {oldPassword, newPassword});
  }

  public remove(userId: string): Observable<string> {
    return this.http.delete(`/api/users/${userId}`)
      .map(() => userId);
  }
}
