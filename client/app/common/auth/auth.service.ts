import { Injectable, EventEmitter, Output } from '@angular/core';
import { UserService } from './user.service';
import { IUser } from '../../../../server/api/user/user.model';
import { Roles } from '../../../../server/common/roles';
import { logger } from '../../utils/logger';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export interface IUserInfo {
  user: IUser;
  isAuthorized: boolean;
  isAdmin?: boolean;
}

export const unauthorizeUser = new EventEmitter<IUser>(true);

@Injectable()
export class AuthService {

  public currentUser: BehaviorSubject<IUserInfo>;

  private defaultUserInfo = {
    user: null,
    isAuthorized: false
  };
  private _currentUser: IUser;

  public constructor(
      private http: HttpClient,
      private userService: UserService,
      public router: Router) {

    this.currentUser = new BehaviorSubject<IUserInfo>(this.defaultUserInfo);
    const token = localStorage.getItem('id_token');
    const user = JSON.parse(localStorage.getItem('user') || '""');

    if (token && user) {
      this.setCurrentUser(user);
    } else {
      this.logout();
    }

    unauthorizeUser.subscribe(() => this.logout());
  }

  public get isAuthorized() {
    return this._currentUser && !! this._currentUser._id;
  }

  public hasRole(role): boolean {
    return this._currentUser &&
      (this._currentUser.role === role || this._currentUser.role === Roles.Admin);
  }

  public changePassword({oldPassword, newPassword}): Observable<void> {
    return this.userService
      .changePassword(this._currentUser._id, oldPassword, newPassword);
  }

  public login(data: { email: string, password: string }): Observable<IUser> {
    return this.http.post<{token: string}>('/auth/local', data)
      .switchMap((tokenInfo) => this.initUser(tokenInfo));
  }

  public createUser(data): Observable<IUser> {
    return this.userService.create(data)
      .switchMap((tokenInfo) => this.initUser(tokenInfo));
  }

  public logout(): Observable<void> {
    localStorage.removeItem('user');
    localStorage.removeItem('id_token');
    this._currentUser = null;
    this.currentUser.next(this.defaultUserInfo);

    return Observable.of(null);
  }

  private initUser({token}): Observable<IUser> {
    localStorage.setItem('id_token', token);
    return this.userService.get()
      .do((user) => {
        this.setCurrentUser(user);
      })
      .catch((err) => Observable.throw(this.logout()));
  }

  private setCurrentUser(user: IUser) {
    localStorage.setItem('user', JSON.stringify(user));
    this._currentUser = user;
    this.currentUser.next({
      user,
      isAuthorized: user && !!user._id,
      isAdmin: this.hasRole(Roles.Admin)
    });
  }
}
