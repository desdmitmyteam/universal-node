import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { logger } from '../../utils/logger';
import 'rxjs/add/operator/do';
import { ToasterService } from 'angular2-toaster';
import { NotifyType } from '../../utils/type';
import { Router } from '@angular/router';
import { IUser } from '../../../../server/api/user/user.model';
import { unauthorizeUser } from './auth.service';

enum ResponceStatus {
  Unauthorized = 401
}

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor {

  public constructor(
    private toaster: ToasterService,
    private router: Router) {
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    logger.data('request', req);

    const token: string = localStorage.getItem('id_token');

    if (token) {
      req = req.clone({headers: req.headers.set('Authorization', `Bearer ${token}`)});
    }

    if (!req.headers.has('Content-Type')) {
      req = req.clone({headers: req.headers.set('Content-Type', 'application/json')});
    }

    req = req.clone({headers: req.headers.set('Accept', 'application/json')});

    return next
      .handle(req)
      .do((ev: HttpEvent<any>) => {
        if (ev instanceof HttpResponse) {
          logger.data('responce', ev);
        }
      })
      .catch((response) => {
        if (response instanceof HttpErrorResponse) {
          if (response.status === ResponceStatus.Unauthorized) {
            logger.error('http error', response);
            if (response.error && response.error.message) {
              this.toaster.pop(NotifyType.Error, null, response.error.message);
            } else {
              this.toaster.pop(NotifyType.Error, 'User Unauthorized', 'Please login');
            }

            unauthorizeUser.emit();
          } else if (response.error) {
            const errors =
              (response.error.errors && Object.keys(response.error.errors).map((key) =>
                ({title: '', message: response.error.errors[key].message})
              )) || [{title: response.statusText, message: response.error.message}];

            errors.forEach((error) =>
              this.toaster.pop(NotifyType.Error, error.title, error.message));

          } else {
            this.toaster.pop(NotifyType.Error, response.statusText, response.message);
          }
        }

        return Observable.throw(response);
      });
  }
}
