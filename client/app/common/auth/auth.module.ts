import { NgModule } from '@angular/core';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { AuthGuardAdmin } from './auth-guard-admin.service';
import { AuthGuardUser } from './auth-guard-user.service';
import { AuthGuard } from './auth-guard.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthHttpInterceptor } from './auth-http-interceptor.service';

@NgModule({
  providers: [
    AuthService,
    UserService,
    AuthGuard,
    AuthGuardAdmin,
    AuthGuardUser,
    {provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true}
  ]
})
export class AuthModule {
}
