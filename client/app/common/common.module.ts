import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthModule } from './auth/auth.module';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { FormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NotAuthGuard } from './auth/not-auth-guard.service';

@NgModule({
  imports: [
    CommonModule,
    CollapseModule,
    AuthModule,
    RouterModule,
    FormsModule
  ],
  declarations: [
    NavbarComponent,
    FooterComponent,
    PageNotFoundComponent
  ],
  providers: [
    NotAuthGuard
  ],
  exports: [
    NavbarComponent,
    FooterComponent,
    PageNotFoundComponent
  ]
})
export class CommonAppModule {
}
