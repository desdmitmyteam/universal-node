import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { ReportsRoutes } from './reports.routes';
import { ReportsService } from './reports.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(ReportsRoutes),
    NgxDatatableModule
  ],
  declarations: [
    ReportsComponent
  ],
  providers: [
    ReportsService
  ]
})
export class ReportsModule {
}
