import { ReportsComponent } from './reports.component';
import { Routes } from '@angular/router';
import { AuthGuard } from '../common/auth/auth-guard.service';

export const ReportsRoutes: Routes = [
  {path: 'report/records', component: ReportsComponent, canActivate: [AuthGuard]}
];
