import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { IRecord } from '../../../server/api/record/record.model';

@Injectable()
export class ReportsService {

  public constructor(private http: HttpClient) {
  }

  public records(): Observable<IRecord[]> {
    return this.http.get<IRecord[]>('/api/records/report');
  }
}
