import { Component, OnInit } from '@angular/core';
import { ReportsService } from './reports.service';
import { IRecord } from '../../../server/api/record/record.model';

@Component({
  selector: 'reports',
  templateUrl: './reports.component.html'
})
export class ReportsComponent implements OnInit {

  public data = [];

  public constructor(
      private reportsService: ReportsService) {
  }

  public ngOnInit() {
    this.reportsService.records().subscribe((data) => {
      this.data = data;
    });
  }
}
