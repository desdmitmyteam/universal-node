import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RecordsComponent } from './records.component';
import { RecordsRoutes } from './records.routes';
import { RecordsService } from './records.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {
  MAT_DATE_LOCALE, MatButtonModule, MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';
import { EditComponent } from './edit/edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(RecordsRoutes),
    NgxDatatableModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  declarations: [
    RecordsComponent,
    EditComponent
  ],
  providers: [
    RecordsService,
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
  ]
})
export class RecordsModule {
}
