import { Component, OnInit } from '@angular/core';
import { RecordsService } from './records.service';
import { IRecord } from '../../../server/api/record/record.model';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { logger } from '../utils/logger';

@Component({
  selector: 'records',
  templateUrl: './records.component.html'
})
export class RecordsComponent implements OnInit {

  public records: IRecord[] = [];

  public constructor(
      private recordsService: RecordsService,
      private router: Router,
      private route: ActivatedRoute) {
  }

  public ngOnInit() {
    this.recordsService.all().subscribe((records) => {
      this.records = records;
    });
  }

  public addRecord() {
    this.router.navigate(['./add'], {relativeTo: this.route});
  }

  public editRecord(rec: IRecord) {
    this.router.navigate([`./edit/${rec._id}`], {relativeTo: this.route});
  }

  public removeRecord(rec: IRecord) {
    this.recordsService.remove(rec).subscribe(() => {
      _.remove(this.records, {_id: rec._id});
      logger.warn('records - remove', rec);
    });
  }
}
