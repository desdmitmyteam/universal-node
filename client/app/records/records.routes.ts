import { RecordsComponent } from './records.component';
import { Routes } from '@angular/router';
import { AuthGuard } from '../common/auth/auth-guard.service';
import { EditComponent } from './edit/edit.component';

export const RecordsRoutes: Routes = [
  {path: 'records', component: RecordsComponent, canActivate: [AuthGuard]},
  {path: 'records/:type', component: EditComponent, canActivate: [AuthGuard]},
  {path: 'records/:type/:id', component: EditComponent, canActivate: [AuthGuard]},
  {path: '', redirectTo: '/records', pathMatch: 'full', canActivate: [AuthGuard]}
];
