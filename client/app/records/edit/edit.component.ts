import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IRecord } from '../../../../server/api/record/record.model';
import { RecordsService } from '../records.service';
import { Observable } from 'rxjs/Observable';
import { record as recordMessage } from '../../../../server/common/messages';
import { DatePipe } from '@angular/common';
import * as _ from 'lodash';
import { logger } from '../../utils/logger';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  public type: string;
  public message = recordMessage;

  public data: IRecord & { time: string } = {
    date: null,
    distance: null,
    period: null,
    time: null
  };

  public constructor(
      private route: ActivatedRoute,
      private recordsService: RecordsService,
      private router: Router) {
  }

  public ngOnInit() {
    this.type = this.route.snapshot.params.type;

    if (this.type === 'edit') {
      this.recordsService.get(this.route.snapshot.params.id).subscribe((record: IRecord) => {
        this.data = _.extend(record, {
          time: new DatePipe('en-US').transform(record.period, 'HH:mm:ss', 'UTC')
        });
      });
    }
  }

  public cancel() {
    this.router.navigate(['/records']);
  }

  public save(form) {
    if (form.valid) {
      this.data.period = this.toMiliseconds(this.data.time);
      this.serviceSave(this.data).subscribe((info) => {
        logger.warn('records - save', info);
        this.router.navigate(['records/']);
      });
    }
  }

  private serviceSave(record: IRecord): Observable<void | IRecord> {
    return record._id
      ? this.recordsService.update(record)
      : this.recordsService.create(this.data);
  }

  private toMiliseconds(time: string): number {
    const data = time.split(':');
    return Number(data[0] || 0) * 3600000 +
      Number(data[1] || 0) * 60000 +
      Number(data[2] || 0) * 1000;
  }
}
