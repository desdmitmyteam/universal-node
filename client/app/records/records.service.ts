import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { IRecord } from '../../../server/api/record/record.model';

@Injectable()
export class RecordsService {

  private url = '/api/records';

  public constructor(private http: HttpClient) {
  }

  public all(): Observable<IRecord[]> {
    return this.http.get<IRecord[]>(this.url);
  }

  public create(record: IRecord): Observable<IRecord> {
      return this.http.post<IRecord>(this.url, record);
  }

  public get(recordId: number): Observable<IRecord> {
      return this.http.get<IRecord>(`${this.url}/${recordId}`);
  }

  public update(record: IRecord): Observable<void> {
      return this.http.put<void>(`${this.url}/${record._id}`, record);
  }

  public remove(record: IRecord): Observable<void> {
    return this.http.delete<void>(`${this.url}/${record._id}`);
  }
}
