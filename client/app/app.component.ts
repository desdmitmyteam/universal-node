import { Component } from '@angular/core';
import { ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'app-root',
  template: `
    <toaster-container [toasterconfig]="toasterconfig"></toaster-container>
    <navbar></navbar>
    <router-outlet></router-outlet>
    <footer></footer>
  `,
  styleUrls: ['./app.scss']
})
export class AppComponent {
  public toasterconfig = new ToasterConfig({
    animation: 'flyRight',
    showCloseButton: true,
    newestOnTop: false,
    mouseoverTimerStop: true,
    limit: 5
  });
}
