echo "Run mongo db"
docker run -itd -p 27017:27017 --name mongo mongo
docker start mongo

echo "Build new image"
docker build --force-rm -t jogging .

echo "Remove all middle images created during the build"
docker rmi $(docker images -f "dangling=true" -q)

echo "Remove old container"
docker rm jogging

echo "Run new container"
docker run -itd --name=jogging --link mongo --env MONGODB_URI=mongodb://mongo/jogging-app --env NODE_ENV=prod -p 80:8080 jogging
