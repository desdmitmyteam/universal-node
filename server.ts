import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import { renderModuleFactory } from '@angular/platform-server';
import { enableProdMode } from '@angular/core';
import * as express from 'express';
import { join } from 'path';
import { appServer } from './server/app';
import { config } from './server/config/environment';
import { LocalStorage } from 'node-localstorage';

if (typeof localStorage === 'undefined' || localStorage === null) {
  global[`localStorage`] = new LocalStorage('./scratch');
}

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

// Init Express server
const app = express();

// Our index.html we'll use as our template
// const template = readFileSync(join(DIST_FOLDER, 'browser', 'index.html')).toString();

// Express Engine
import { ngExpressEngine } from '@nguniversal/express-engine';
// Import module map for lazy loading
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
import { routes } from './server/routes';

if (process.env.NO_HTML !== 'true') {
  // * NOTE :: leave this as require() since this file is built Dynamically from webpack
  const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require('./dist/server/main.bundle');
  // Express-engine (https://github.com/angular/universal/tree/master/modules/express-engine)
  app.engine('html', ngExpressEngine({
    bootstrap: AppServerModuleNgFactory,
    providers: [
      provideModuleMap(LAZY_MODULE_MAP)
    ]
  }));
}

// Html view engine is set to Universal engine
app.set('view engine', 'html');
app.set('views', join(config.distFolder, 'browser'));

// Setup Express server settings
const startServer = appServer(app);

// Setup Express server routes
routes(app);

// ALl regular routes use the Universal engine / Html view engine
app.get('*', (req, res) => {
  res.render('index', {req});
});

// Start Express server
startServer();
